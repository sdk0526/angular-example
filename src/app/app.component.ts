import { Component } from '@angular/core';
import { fadeAnimation, slideInDownAnimation } from './animation';
import { AuthenticationService } from './services/authentication.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [fadeAnimation] // register the animation
})
export class AppComponent {
  title = 'app';
  constructor(
    private auth: AuthenticationService,
    private toast: ToastrService) {}
  logout() {
    this.auth.logOut();
  }
}
