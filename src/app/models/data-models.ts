export interface SignInData {
    name: string;
    password: string;
}

export interface User {
    id: number;
    token?: string;
}

export interface AppVersion {
    id: number;
    appId: number;
    vName?: string;
    shortDescription?: string;
    longDescription?: string;
    vIcon?: string;
    screenShoots?: string[];
}

export interface App {
    id: number;
    name?: string;
    icon?: string;
    company?: string;
}
