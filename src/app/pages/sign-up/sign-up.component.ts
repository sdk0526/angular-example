import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from './../../services/authentication.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  signUpCredentials: { userName: string, password: string, verifyPassword: string };
  constructor(
    private toast: ToastrService,
    private auth: AuthenticationService,
    private router: Router) {
    this.signUpCredentials = {userName: '', password: '', verifyPassword: '' };
  }

  ngOnInit() {
  }

  signUp() {
    console.log(this.signUpCredentials.password + '---' + this.signUpCredentials.verifyPassword);
    if (this.signUpCredentials.password !== this.signUpCredentials.verifyPassword) {
      this.toast.error('The password and verify password fields do not match', 'Fields Error', {timeOut: 4000});
      return;
    }
    this.auth.signUp(this.signUpCredentials.userName, this.signUpCredentials.password);
    this.router.navigate(['/signIn']);
  }

}
