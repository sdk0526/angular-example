import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  userName: string;
  password: string;
  constructor(private toast: ToastrService, private auth: AuthenticationService, private rout: Router) { }

  ngOnInit() {
  }

  logIn() {
    this.auth.logIn(this.userName, this.password);
    this.rout.navigate(['/myPage']);
  }

}
