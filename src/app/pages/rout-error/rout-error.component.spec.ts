import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutErrorComponent } from './rout-error.component';

describe('RoutErrorComponent', () => {
  let component: RoutErrorComponent;
  let fixture: ComponentFixture<RoutErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
