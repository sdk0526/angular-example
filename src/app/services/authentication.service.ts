import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AuthenticationService {
  logedIn = false;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };
  constructor(private http: HttpClient) { }


  isLogedIn(): boolean {
    const isLogedIn = localStorage.getItem('isLogedIn');
    return isLogedIn && JSON.parse(isLogedIn) === true;
  }

  logIn(user: string, password: string): boolean {
    // TODO: change after you build the api
    this.setToken('sldkghaldskghlakadflg');
    this.logedIn = false;
    return true;
  }

  setUser(user: string): void {
    localStorage.setItem('smUser', user);
  }

  getUser(): string {
    const user = localStorage.getItem('smUser');
    if (user) {
      return user;
    }
    return 'Guest';
  }

  logOut(): boolean {
    localStorage.setItem('smUser', null);
    localStorage.setItem('smToken', null);
    this.logedIn = false;
    // TODO: change after you build the api
    return true;
  }

  signUp(user: string, password: string): boolean {
    // TODO: change after you build the api
    return true;
  }

  getToken(): string {
    const token = localStorage.getItem('smToken');
    if (token) {
      return token;
    }
    return '';
  }

  setToken(token: string): void {
    localStorage.setItem('smToken', token);
  }
}
