import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };
  constructor(private http: HttpClient) { }

  getExample() {
    this.http.get('http://www.localhost:5000/api/v1.0/users', this.httpOptions).subscribe(
      succ => console.log(succ),
      error => console.log(error));
  }
}
