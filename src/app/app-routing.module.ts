import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { MyPageComponent } from './pages/my-page/my-page.component';
import { EditAppComponent } from './pages/edit-app/edit-app.component';
import { RoutErrorComponent } from './pages/rout-error/rout-error.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'signIn', component: SignInComponent},
  {path: 'signUp', component: SignUpComponent},
  {path: 'myPage', component: MyPageComponent},
  {path: 'EditApp/:{appId}', component: EditAppComponent},
  {path: '**', component: RoutErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
